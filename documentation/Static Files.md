## Static Resources (`.css`, `.js`, `.img`, etc)
Find/Create them under `src/main/webapp/resources`
To access them when running the service use `localhost:8080/resources/...`
Example: `localhost:8080/resources/bootstrap/css/bootstrap.css` will display the css in that file

## Static Resources (`.html`, `.htm`, etc)
Find/Create them under `src/main/webapp/web`
To access them when running the service use `localhost:8080/web/...`
Example: `localhost:8080/web/login.html` will display the page
**Important**
For the CSS and JS to be 'visible' to the web page (html) you will need to reference the files like
`/resources/xxx.css` or `/resources/yyy.js` in the html file
Example: `<link rel="stylesheet" href="/resources/css/font-awesome.min.css">`
By using "/resources/" at the beginning the page knows to use the same URL but start at the resources directory.