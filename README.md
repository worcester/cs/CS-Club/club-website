## Club-Website
    This project is for the set up and developement of the club's website. 
    The site will be used to host club resources, projects, info etc. 
    At the moment we have the code from a them added to the repo to serve as a 
    launching off point.  Feel free to download the files yourself at this point, 
    we're going to beign working on the site whenever we can.

### Getting Started
    To start reviewing the files and working you'll need to [fork the project](http://guides.github.com/activities/forking/)
    to your own namespace and create a [new branch](https://guides.github.com/activities/hello-world/)
    to work in.  Afterwards, use the *git bash* terminal to clone the 
    files onto your own pc. From there you can work on the files and push them 
    back to your own workspace.  Once something needs to be added to the master
    branch you can request to merge it.

### Prerequisites
    You'll need a gitlab account and have an ssh configured to begin.  As far 
    as IDEs go we don't have any restrictions although I(Jacob) recomend webstorm
    or pycharm (https://www.jetbrains.com/). 
    

## License
    This project is licensed under the MIT license.  For more info check our license
    file.
    