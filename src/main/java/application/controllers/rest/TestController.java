/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.controllers.rest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@Api(value = "TEST CONTROLLERS", tags = { "TEST CONTROLLERS" })
public class TestController {
	private static Logger logger = Logger.getLogger(TestController.class);

	@RequestMapping(value = "v1/test", method = RequestMethod.GET)
	public String recieveSensorData() {
		logger.debug("Hit test endpoint.");
		return "Test Passed.";
	}
}
