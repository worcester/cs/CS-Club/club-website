/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.services.accessors;

import java.util.UUID;

import application.models.request.user.UserPostRequest;
import application.models.request.user.UserUpdateRequest;
import application.models.response.user.User;
import application.services.QualityAssuranceDBIAccessor;
import application.services.exceptions.DBICreationException;

public interface UserAccessor {

	public static User getById(Integer id) throws DBICreationException {
		User object = null;
		return object;
	}

	public static User getByUuid(String uuid) throws DBICreationException {
		User object = null;
		return object;
	}

	public static User getByUsername(String username) throws DBICreationException {
		User object = null;
		return object;
	}

	public static User post(UserPostRequest request) throws DBICreationException {

		// TODO Validate Input
		String user_id = UUID.randomUUID().toString();

		User response = null;
		// TODO post

		return response;
	}

	public static User updateUser(UserUpdateRequest request) throws DBICreationException {
		User user = getByUsername(request.getUsername());

		// TODO Validate Input
		if (request.getUsername() != null) {
			user.setUsername(request.getUsername());
		}
		if (request.getPassword() != null) {
			user.setPassword(request.getPassword());
			// TODO BCrypt
		}
		if (request.getGiven_name() != null) {
			user.setGiven_name(request.getGiven_name());
		}
		if (request.getSurname() != null) {
			user.setSurname(request.getSurname());
		}

		// TODO Update User
		User response = null;

		return response;
	}

	public static User delete(String uuid) throws DBICreationException {
		User response = getByUuid(uuid);

		// TODO delete

		return response;
	}
}
